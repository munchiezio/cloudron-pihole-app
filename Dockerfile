FROM cloudron/base:1.0.0

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

#EXPOSE 53 53/udp
#EXPOSE 67/udp
#EXPOSE 80
#EXPOSE 443

ENV QUERY_LOGGING=true
ENV INSTALL_WEB_INTERFACE=true
ENV PRIVACY_LEVEL=0
ENV IPV6_ADDRESS=""
ENV PIHOLE_DNS_1="127.0.0.1"
ENV QUERY_LOGGING=true

RUN rm -rf /var/run/s6 && mkdir -p /run/var/run/s6 && ln -fs /run/var/run/s6 /var/run/s6
RUN mkdir -p /app/data/etc && \ 
    cp -r /etc/ /app/data/etc/ && \
    rm -rf /etc && \
    ln -fs /app/data/etc/ /etc

ENV S6_READ_ONLY_ROOT=1
ENV S6OVERLAY_RELEASE https://github.com/just-containers/s6-overlay/releases/download/v1.21.7.0/s6-overlay-amd64.tar.gz
COPY install.sh /usr/local/bin/install.sh
COPY VERSION /etc/docker-pi-hole-version
ENV PIHOLE_INSTALL /root/ph_install.sh

ENV ServerIP="${CLOUDRON_PROXY_IP:-'0.0.0.0'}"

RUN bash -ex install.sh 2>&1 && \
    rm -rf /var/cache/apt/archives /var/lib/apt/lists/*

ENTRYPOINT [ "/s6-init" ]

ADD s6/debian-root /
COPY s6/service /usr/local/bin/service

# php config start passes special ENVs into
ENV PHP_ENV_CONFIG '/etc/lighttpd/conf-enabled/15-fastcgi-php.conf'
ENV PHP_ERROR_LOG '/var/log/lighttpd/error.log'
COPY ./start.sh /
COPY ./bash_functions.sh /

# IPv6 disable flag for networks/devices that do not support it
ENV IPv6 True

ENV S6_LOGGING 0
ENV S6_KEEP_ENV 1
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2
ENV FTL_CMD no-daemon

ENV VERSION v4.2.1
ENV ARCH amd64
ENV PATH /opt/pihole:${PATH}

HEALTHCHECK CMD dig @127.0.0.1 pi.hole || exit 1

SHELL ["/bin/bash", "-c"]